#include <stdio.h>
#include <stdlib.h>

int main ()
{
  // Pide una cantidad N de numeros, los ingresa a un arreglo y busca el mayor
  int tamArreglo, nMayor;

  printf("Programa que pide una cantidad N de numeros, los ingresa a un arreglo y busca el mayor\n");

  printf("Ingrese el tamaño del arreglo: ");
  scanf("%i", &tamArreglo);

  int arreglo[tamArreglo];

  for (int i=0;i<tamArreglo; i++)
  {
    printf("Ingresa numero %i: ",i+1);
    scanf("%i", &arreglo[i]);
    if (tamArreglo == 0) nMayor = arreglo[i];
    if (arreglo[i] > nMayor) nMayor = arreglo[i];
  }

  printf("\nEl numero mayor de cada uno de los numeros de los arreglos es: %i\n", nMayor);

  printf("-----------\n");
  return 0;
}
