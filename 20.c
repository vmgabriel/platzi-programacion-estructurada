#include <stdio.h>
#include <stdlib.h>

float calificacion;
char nombre[];

void mensajeAprobado()
{
  if (calificacion >= 7)
  {
    printf("Enhorabuena, el alumno ");
    puts(nombre);
    printf(" ha aprobado con una calificacion de %f",calificacion);
  }
  else
  {
    printf("Lo siento, el alumno ");
    puts(nombre);
    printf(" NO ha aprobado, su calificacion es %f",calificacion);
  }
}

int main ()
{
  // Verificar si un alumno esta aprobado o no

  printf("Programa que evalua si un alumno esta aprobado o no\n");

  printf("Ingrese nombre del alumno: ");
  gets(nombre);

  printf("Ingrese Calificacion del alumno");
  scanf("%f", &calificacion);

  mensajeAprobado();

  printf("-----------\n");
  return 0;
}
