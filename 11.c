#include <stdio.h>
#include <stdlib.h>

int main ()
{
  // Secuencia Fibbonaci
  int t0 = 0, t1 = 1, tn;
  int limite;

  printf("Programa que genera la secuencia fibonacci\n");

  printf("Ingrese la cantidad de Terminos: ");
  scanf("%i", &limite);

  printf("0\n1\n");

  for (int i=0;i<(limite-2);i++)
  {
    tn = t1 + t0;
    t0 = t1;
    t1 = tn;
    printf("%i\n", tn);
  }

  printf("-----------\n");
  return 0;
}
