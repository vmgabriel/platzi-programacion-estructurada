#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main ()
{
  // Ingresa un string, este invertira
  char varEntrada[50];
  int tam;

  printf("Programa que al string que se le suministra se le invierte\n");

  printf("Ingrese la palabra: ");
  gets(varEntrada);
  tam = strlen(varEntrada);

  for(int i=(tam-1);i==0;i--)
  {
    printf("%c", varEntrada[i]);
  }
  printf("\n");

  printf("-----------\n");
  return 0;
}
