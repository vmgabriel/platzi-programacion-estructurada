#include <stdio.h>
#include <stdlib.h>

float USDtoCOP(float dinero)
{
  return dinero * 3147.37;
}

float COPtoUSD(float dinero)
{
  return dinero * 0.000318;
}

int main ()
{
  // Conversion de Cambio de Divisa
  float dinero, cambio;

  printf("Convierte de Dolares(USD) a Pesos(COP) y de Pesos(COP) a Dolares(USD)\n");

  printf("Ingrese Valor a hacer el cambio: ");
  scanf("%f", &dinero);

  printf("\nSeleccione\n");
  printf("1, Pesos a Dolares\n");
  printf("2, Dolares a Pesos\n");

  printf("Ingrese Seleccion(1 o 2): ");
  scanf("%i", &cambio);

  if (cambio == 1)
  {
    printf("El cambio de %f Pesos(COP) a Dolares(USD) es: %f\n",dinero,COPtoUSD(dinero));
  }
  else
  {
    printf("El cambio de %f Dolares(USD) a Pesos(COP) es: %f\n",dinero,USDtoCOP(dinero));
  }

  printf("-----------\n");
  return 0;
}
