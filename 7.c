#include <stdio.h>
#include <stdlib.h>

int main ()
{
  // Ingresa dos numeros, halla el menor de dos numero y lo imprime
  float x, y;

  printf("Programa que halla el menor de dos numeros\n");

  printf("Ingrese valor 1: ");
  scanf("%f", &x);

  printf("Ingrese valor 2: ");
  scanf("%f", &y);

  if ( x < y)
  {
    printf("El primer numero es menor: %f\n", x);
  }
  else
  {
    printf("El Segundo numero es menor: %f\n", y);
  }

  printf("-----------\n");
  return 0;
}
