#include <stdio.h>
#include <stdlib.h>

int main ()
{
  // a X que es 10 sumarle el doble de su valor
  int x = 10;

  printf("Programa que suma a X que es 10 con el doble de su valor\n");

  x += 2*x;

  printf("El valor es: %i\n",x);

  printf("-----------\n");
  return 0;
}
