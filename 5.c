#include <stdio.h>
#include <stdlib.h>

int main ()
{
  // Ingresar un numero entero usando operadores de asignacion usar modulo 5 y sumarle 1
  int x;

  printf("Programa que a un numero ingresado saca el modulo 5 y suma 1\n");

  printf("Ingrese valor: ");
  scanf("%i", &x);

  x %= 5;

  printf("El valor es: %i\n",++x);

  printf("-----------\n");
  return 0;
}
