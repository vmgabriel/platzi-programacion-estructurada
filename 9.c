#include <stdio.h>
#include <stdlib.h>

int main ()
{
  // Juego donde tu decision cambia la historia
  int decision;

  printf("El camino del Heroe\n\n");

  printf("------\n");
  printf("En el camino tu personaje se topa con unos enemigos de muy alto nivel un troll, un gigante y un orco, tienes poca vida, y estas a 100 de mana\n");
  printf("1. Ataco con espada a gigante, lanzo flecha a troll y ataca con magia a el orco\n");
  printf("2. Ataco con espada al orco, enfrento con magia a troll y lanzo flecha a gigante\n");
  printf("3. Ataco con espada al gigante, lanzo flecha al orco y ataca con magia al troll\n\n");
  printf("Que hara tu personaje??\n");
  printf("------\n");

  printf("Selecciona una de las tres opciones(1, 2 o 3): ");
  scanf("%i", &decision);
  printf("\n");

  switch(decision)
  {
  case 1:
    {
      printf("El gigante cae derrotado de tu habilidad con el arma, al lanzar la flecha al troll este se escuda con su mano y ataca a tu personaje con un fiero golpe causandote la muerte\n");
      printf("Game Over!!\n");
    }
    break;
  case 2:
    {
      printf("El Orco responde tu golpe con su arma, esto no te permite lanzar la flecha, el gigante te golpea severamente y caes derrotado\n");
      printf("Game Over!!\n");
    }
    break;
  case 3:
    {
      printf("El gigante cae derrotado de tu habilidad con el arma, al lanzar la flecha al orco cae directamente en la cabeza dejandolo derrotado, el troll viendose abrumado en tan imponente magia cae derrotado, El heroe gana mucha experiencia.\n");
      printf("Haz Ganado!!\n");
    }
    break;
  default:
    {
      printf("Al no reaccionar, tu personaje fue triturado por el gigante, fue golpeado por el troll y el orco te trituro\n");
      printf("Game Over!!\n");
    }
  }

  printf("-----------\n");
  return 0;
}
