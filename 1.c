#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h> // Libreria Para Booleanos

int main ()
{
  // Tipos de datos sencillos de C
  int valor = 1;
  float floatA = 1.2;
  double doubleA = 1.121212;
  char charA = 'C';

  // Uso de Print para imprimir, imprimir un int
  printf("--------\n");
  printf("El Valor valor es: %i\n", valor);
  printf("--------\n");

  // Uso de Print para imprimir, imprimir un float
  printf("--------\n");
  printf("El Valor valor es: %f\n", floatA);
  printf("--------\n");

  // Uso de Print para imprimir, imprimir un double
  printf("--------\n");
  printf("El Valor valor es: %f\n", doubleA);
  printf("--------\n");

  // Uso de Print para imprimir, imprimir un caracter
  printf("--------\n");
  printf("El Valor valor es: %c\n", charA);
  printf("--------\n");

  // Uso de print concadenado, imprime char, double y int
  printf("--------\n");
  printf("El caracter es: %c, el double es %f, el integer es: %i\n", charA,doubleA,valor);
  printf("--------\n");

  // Uso de libreria Booleana, no es estandar en C
  bool a = true;
  int edadUsuario;

  // Entrada de Datos, uso de scanf
  printf("--------\n");
  printf("Ingrese Edad: ");
  scanf("%i", &edadUsuario); //Usamos la variable como si fuera un apuntador
  printf("--------\n");

  printf("--------\n");
  printf("El Dato Ingresado es %i y el boleano es %b\n", edadUsuario, a);


  // Codigo para ingreso de dos variables y las intercambia de posicion
  int varA, varB; // Variables para entrada de datos
  int varC; // Variables para procesamiento

  printf("Ingresa el valor de la variable X: ");
  scanf("%i", &varA);

  printf("Ingresa el valor de la variable Y: ");
  scanf("%i", &varB);
  printf("\n"); //Un estetico Salto de linea

  // Procesamiento del programa
  varC = varA; // Uso de la variable auxiliar
  varA = varB;
  varB = varC;

  //Imprimir Variables
  printf("Las variables intercambiadas en X: %i\n", varA);
  printf("Las variables intercambiadas en Y: %i\nn", varB);


  return 0;
}
