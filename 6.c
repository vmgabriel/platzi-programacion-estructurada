#include <stdio.h>
#include <stdlib.h>

int main ()
{
  // Ingresar un numero del 1 al 10, si es 5 adivino
  int x, valorAdivina = 5;

  printf("Programa de adivinanza\n");

  printf("Ingrese valor( 1 al 10 ): ");
  scanf("%i", &x);

  if (x == valorAdivina)
  {
    printf("Es correcto adivinaste!!!\n");
  }
  else
  {
    printf("Lo siento, numero equivocado\n");
  }

  printf("-----------\n");
  return 0;
}
