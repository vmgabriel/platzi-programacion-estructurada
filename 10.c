#include <stdio.h>
#include <stdlib.h>

int main ()
{
  // Imprime "#" en 5 filas
  int i=0;

  printf("Programa de Impresion # en 5 filas\n");

  while (i < 5)
  {
    printf("#\n");
    i++;
  }

  printf("-----------\n");
  return 0;
}
