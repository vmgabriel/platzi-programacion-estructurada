#include <stdio.h>
#include <stdlib.h>

int main ()
{
  // Ingresar la calificacion de un alumno
  // menor 60 -> reprobo
  // mas o igual 60 -> aprobado
  // mas de 90 -> Aprobado con carita feliz
  float calificacion;

  printf("Programa de verificacion de calificacion de un alumno\n");

  printf("Ingrese calificacion: ");
  scanf("%f", &calificacion);

  if (calificacion >= 60)
  {
    printf("El alumno esta aprobado, con un puntaje de %f", calificacion);

    if (calificacion >= 90)
    {
      printf("  n.n / \n");
    }
    else
    {
      printf("\n");
    }
  }
  else
  {
    printf("El alumno ha reprobado, con un puntaje de %f\n", calificacion);
  }

  printf("-----------\n");
  return 0;
}
