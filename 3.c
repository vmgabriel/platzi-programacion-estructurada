#include <stdio.h>
#include <stdlib.h>

#define PI 3.14159265358979323846264338 // Definimos PI

int main ()
{
  // Calcular la conversion de grados fahrenheit a celcius
  float farenheit, celsius;

  printf("Programa para convertir grados fahrenheit a celcius\n");

  printf("Ingrese Los grados fahrenheit: ");
  scanf("%f", &farenheit);

  celsius = ((farenheit - 32) / 9) * 5;

  printf("Conversion de %f grados fahrenheit a celsius: %fC \n", farenheit, celsius);

  printf("-----------\n");

  return 0;
}
