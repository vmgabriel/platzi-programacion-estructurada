#include <stdio.h>
#include <stdlib.h>

int main ()
{
  // Imprime los primeros 100 numeros naturales
  int arreglo[5], res = 1;

  printf("Programa que multiplica de un arreglo de datos suministrados por usuario\n");

  for (int i=0;i<5;i++)
  {
    printf("Ingresa numero %i: ",i+1);
    scanf("%i", &arreglo[i]);
    res *= arreglo[i];
  }

  printf("\nEl resultado de la multiplacion del arreglo seria: %i\n", res);

  printf("-----------\n");
  return 0;
}
