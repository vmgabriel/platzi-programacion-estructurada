#include <stdio.h>
#include <stdlib.h>

#define PI 3.14159265358979323846264338 // Definimos PI

int main ()
{
  // Calcular el area de un Cilindro
  float radio, altura, area, volumen;

  printf("Programa para calcular el area de un cilindro\n");

  printf("Ingrese Radio de la Base del Cilindro(en Metros): ");
  scanf("%f", &radio);

  printf("Ingrese Altura del Cilindro(en Metros): ");
  scanf("%f", &altura);

  area = 2 * PI * radio * (radio + altura);
  volumen = PI * (radio * radio) * altura;

  printf("Area del Cilindro: %f metros cuadrados\n", area);
  printf("Volumen del Cilindro: %f metros cubicos\n", volumen);

  printf("-----------\n");

  return 0;
}
