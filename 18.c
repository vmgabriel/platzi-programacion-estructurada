#include <stdio.h>
#include <stdlib.h>

int potencia(int base, int exponente)
{
  if (exponente == 0) return 1;
  return base * potencia(base, exponente-1);
}

int main ()
{
  // Calculo de Potencia con funcion
  int base, exponente;

  printf("Programa de adivinanza\n");

  printf("Ingrese base: ");
  scanf("%i", &base);

  printf("Ingrese Exponente: ");
  scanf("%i", &exponente);

  printf("\n");

  printf("%i^%i = %i\n",base,exponente,potencia(base,exponente));

  printf("-----------\n");
  return 0;
}
