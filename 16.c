#include <stdio.h>
#include <stdlib.h>

int main ()
{
  // Control de Notas de arreglo bidimensional 5 filas, 6 columnas
  float listaNotas[5][6];

  printf("Programa que calcula el promedio de cada una de las filas\n");

  for(int i=0;i<5;i++)
  {
    listaNotas[i][5] = 0;
    for (int j=0;j<5;j++)
    {
      printf("Ingresa La Nota(Entre 6 y 10) %i del Estudiante Numero %i: ",j+1,i+1);
      scanf("%f", &listaNotas[i][j]);
      listaNotas[i][5] += listaNotas[i][j];
    }
    listaNotas[i][5] /= 5;
    printf("\t\tPromedio de Nota para el Estudiante Numero %i es: %f\n",i+1,listaNotas[i][5]);
  }

  printf("-----------\n");
  return 0;
}
