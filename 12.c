 #include <stdio.h>
#include <stdlib.h>

int main ()
{
  // Imprime los primeros 100 numeros naturales
  int n=1;

  printf("Programa que imprime los primeros 100 numeros naturales\n");

  do
  {
    printf("%i\n",n);
    n++;
  } while(n <= 100);

  printf("-----------\n");
  return 0;
}
