#include <stdio.h>
#include <stdlib.h>

int main ()
{
  // Arreglos bidimensionales 3 filas 4 columnas
  // primera fila -> suma 4
  // segunda fila -> suma 10
  // tercera fila -> suma 26
  int sum1fila = 0, sum2fila = 0, sum3fila = 0, i;

  int arreglo[3][4] = {
                       {1,1,1,1},
                       {1,2,3,4},
                       {6,6,6,8}
  };

  printf("Programa de arreglos bidimensionales\n");

  for (i=0;i<4;i++)
  {
    sum1fila += arreglo[0][i];
    sum2fila += arreglo[1][i];
    sum3fila += arreglo[2][i];
  }

  printf("La suma de la primera fila es: %i\n",sum1fila);
  printf("La suma de la segunda fila es: %i\n",sum2fila);
  printf("La suma de la tercera fila es: %i\n",sum3fila);

  printf("-----------\n");
  return 0;
}
